# Drutopia VM

Drutopia VM is a DrupalVM-powered virtual machine system intended to provide a
complete Drutopia system with minimal effort (and maximum flexibility). In
addition, tools are provided within the VM to help you to easily save your
configuration out of your local system and also push that configuration to a
hosted instance Drutopia.


# Getting started

To use Drutopia VM, you will need both VirtualBox and Vagrant. These provide a
system which can emulate a complete system within it's own container. This
contained system can communicate directly with your local system, and shares
its content with the Drutopia-VM project's folder (within the virtual machine,
this appears at /vagrant - but is also at and should be used from /var/www
..?).

* [VirtualBox.org](https://www.virtualbox.org/)
* [VagrantUp.com](https://www.vagrantup.com/)

Once both of those are installed:
  * Git clone the project: `git clone git@gitlab.com:drutopia/drutopia_vm.git
    [folder]` (folder is optional and specifies the name of the folder to clone
    the project into. Otherwise, it is cloned into the drutopia_vm folder.)
    Note that the name of the folder you clone into is used for the name of the
    virtual machine automatically. This then also defines the host name it will
    receive. For example, putting the file in a folder `my_site` will make a
    machine called `my-site-vm` which can be reached at `http://my-site.test`
  * Optional, but strongly recommended: Configure your initial settings edit
    the provisioning/box/config.yml file to suit your project and preferences.
  * run `vagrant up` in the folder you cloned into.
  * access your VM at the URL reported at completion of provisioning. (The
    default would be http://www.drutopia-vm.test)
  * access the VM via command like with `vagrant ssh`

# Troubleshooting

  * With VM provisioning:
    * If you experience any issues with provisioning via `vagrant up`, you can
      rerun provisioning with `vagrant provision`. This may occur if you have
      errors in your config.yml. If a first instance of provisioning has
      failed, it may be necessary to restart the apache server with `sudo
      apache2ctl graceful`.

# Syncing with Drutopia

If you specify a your member name in the `drutopia_member` config.yml variable,
then drush should be configured inside the VM to support the aliases
`drutopia.live` and `drutopia.test`. Additionally, rake is supplied inside of
the VM, which makes use of drush even simpler. You can use rake to pull your
current database from your live server to the local db. For example: `rake
db_sync_live_to_local`

# Deploying to Drutopia (Currently unsupported)
Drutopia VM contains a set of ansible scripts that enable you to push your
Drutopia site to a Drutopia hosting instance. These scripts are configured to
be able to run from within the VM instance from within the
/vagrant/provisioning/drutopia folder. The VM instance will contain all
required build tools, but if you have ansible on your host machine, you may
also run these scripts provided you have the build requirements available there
as well (e.g. composer, sassc).

To configure provisioning, you must first:
  * Create an inventory file. This file contains a list of deployment targets
    (i.e. where you will push your site to). Understanding group_vars, host
    groups, and so on are beyond the scope of this document. Visit
    http://ansible.com to learn more. For purposes of this guide, follow the
    example in provisioning/drutopia/inventory_test. One might simply copy the
    inventory file and test host_vars folder, and adjust accordingly.
  * Set host_var settings. Again, here, please follow the examples provided.
    WARNING: Do NOT commit your host_var settings to a public code repository
    without first understanding the Ansible vault guide. A sample vault
    password file has been provided. Create your own file, outside of the
    project root (or add to your .gitignore, as this file does not belong in
    the same version controlled project). This file corresponds with the
    --vault-password-file option.
  * Note that a default deploy pushes the master branch from the source, not
    your current branch. You may override this with the git_branch setting in
    the host_var file.
  * Set up drush aliases. Be sure your drush alias correspond to the host_var
    values.
  * Run a deploy: `ansible-playbook -i inventory_test deploy.yml
    --vault-password-file`

# Drush aliases

Drush aliases enable you to communicate easily with various Drupal instances.
The folder /vagrant/drush contains alias files that allow you to define the
connection to your instances.

# Ansible vaults

Following the sample host_var files, you can see that a vault value can be used
to 'fill in' the value of a variable, keeping private configuration data safe.
