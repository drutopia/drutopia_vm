# Updating DrutopiaVM

As Drutopia VM is a DrupalVM-powered virtual machine, we shall occasionally
pull in the latest version of the upstream DrupalVM. The entire DrupalVM repo
can be copied to under the box/ directory.

Very minor patching to DrupalVM is required in order to support the automatic
naming of the virtual machine, according to the folder structure.

In the Vagrant file, immedately following the vconfig assignment via
load_config, you must add the following lines:

```
# Override all drupalvm.test values according to project root:
require (File.expand_path('drutopiavm', host_config_dir))
set_overrides_per_project(vconfig)
```
